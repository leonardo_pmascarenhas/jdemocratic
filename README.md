# JDemocratic
Simple app to test Play Framework and Play Authenticate.

## Objective
The objective here, is provide a way for a group of users to vote in a place to lunch. Respecting the rules below:

* You need to vote until 11:45 AM or after that for the next day
* The selected restaurant can be the winner just once a week
* And of course, you can vote just once a day ;D

## Instructions to run

### Create a mySQL database named "jdemocratic" or change the configuration in:
```
conf/application.conf
```

### Install play framework
[Download and instructions](https://www.playframework.com/download)

### Use the command line
Run the project from its directory:

*The first time this takes a while, is when the Apache Ivy (via sbt) implement managed dependencies*
```
activator run
```

Enter the interactive cli (in project directory):
```
activator
```

**if you have some issues with dependencies or if the sbt cache is corrupted, clean and try again**
```
activator clean
```