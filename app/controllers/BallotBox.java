package controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.soap.SOAPBinding.Use;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import models.Restaurant;
import models.User;
import models.Vote;
import models.Winner;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import resources.Utils;

public class BallotBox extends Controller {
	
	final static Form<Vote> voteForm = Form.form(Vote.class);
	
	@Restrict(@Group(Application.USER_ROLE))
	public static Result manager() {
		List<Vote> userVotes = Vote.listToManager();
		return ok(views.html.ballotbox.manager.render(userVotes));
	}
	
	@Restrict(@Group(Application.USER_ROLE))
	public static Result create() {
		final User localUser = Application.getLocalUser(session());
		final List<Restaurant> restaurants = Restaurant.all();
		
		return ok(views.html.ballotbox.create.render(voteForm, localUser, restaurants));
	}
	
	@Restrict(@Group(Application.USER_ROLE))
	public static Result save() {
		Form<Vote> form = voteForm.bindFromRequest();
		final User localUser = Application.getLocalUser(session());
		final List<Restaurant> restaurants = Restaurant.all();

		if (form.hasErrors()) {
			flash("error", "Error");
			return badRequest(views.html.ballotbox.create.render(form, localUser, restaurants));
		}
		
		if (!Vote.userCanVote(localUser)) {
			flash("error", "Sorry, but you cannot vote more than once a day.");
			return redirect(routes.BallotBox.create());
		}
		
		if (!Utils.isValidElectionCycle()) {
			flash("error", "Sorry, but the voting cycle ended. Come back tomorrow ;D");
			return redirect(routes.BallotBox.create());
		}
		
		Long restaurantId = form.get().getRestaurant().getId();
		final Restaurant restaurant = Restaurant.findById(restaurantId);
		Vote userVote = new Vote(new Date(), localUser, restaurant);
		userVote.save();
		
		flash("success", "success");
		return found(routes.BallotBox.manager());
	}
	
	public static Result result() {
		Winner winner = Winner.getWinnerDay();
		return ok(views.html.ballotbox.result.render(winner));
	}
	
}
