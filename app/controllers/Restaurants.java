package controllers;

import java.util.List;

import models.Restaurant;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;

public class Restaurants extends Controller {

	final static Form<Restaurant> restaurantForm = Form.form(Restaurant.class);

	@Restrict(@Group(Application.USER_ROLE))
	public static Result manager() {
		List<Restaurant> restaurants = Restaurant.all();
		return ok(views.html.restaurant.manager.render(restaurants));
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result create() {
		return ok(views.html.restaurant.create.render(restaurantForm));
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result edit(Long id) {
		Restaurant restaurant = Restaurant.findById(id);
		Form<Restaurant> form = restaurantForm.fill(restaurant);
		return ok(views.html.restaurant.edit.render(form, id));
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result save() {
		Form<Restaurant> form = restaurantForm.bindFromRequest();
		if (form.hasErrors()) {
			return badRequest(views.html.restaurant.create.render(form));
		}

		Restaurant restaurant = form.get();
		restaurant.save();

		flash("success", "success");

		return found(routes.Restaurants.manager());
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result update(Long id) {
		Form<Restaurant> form = restaurantForm.bindFromRequest();
		if (form.hasErrors()) {
			return badRequest(views.html.restaurant.edit.render(form, id));
		}

		Restaurant restaurant = form.get();
		restaurant.setId(id);
		restaurant.update();

		flash("success", "success");

		return found(routes.Restaurants.manager());
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result delete(Long id) {
		Restaurant restaurant = Restaurant.findById(id);

		if (restaurant == null) {
			flash("error", "error");
			return found(routes.Restaurants.manager());
		}

		restaurant.delete();
		flash("success", "success");

		return found(routes.Restaurants.manager());
	}
}
