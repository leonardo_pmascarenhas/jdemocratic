package models;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import resources.Utils;

import com.avaje.ebean.Ebean;

@Entity
@Table(name = "restaurants")
public class Restaurant extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	@Required
	private String name;
	private String address;
	private String email;
	private int phone;
	
	public Restaurant() {
		super();
	}

	public Restaurant(String name, String address, int phone) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}
	
	
	
	/* 
	 * Static Resources 
	 */ 

	public static Model.Finder<String, Restaurant> find = new Model.Finder<String, Restaurant>(String.class, Restaurant.class);
	
	public static List<Restaurant> all() {
		return find.all();
	}
	
	public static Restaurant findById(Long id) { 
		return find.where().eq("id", id).findUnique();
	}

	public static List<Restaurant> findByName(String name) {
		return find.where().eq("name", name).findList();
	}

	/*
	 * Count the number of votes
	 */
	public int voteCounter() {
		Calendar calInit = Utils.getVoteCycleInit();
		Calendar calEnd = Utils.getVoteCycleEnd();
		int total = Ebean.find(Vote.class).where().eq("restaurant", this).between("date", calInit.getTime(), calEnd.getTime()).findRowCount();
		
		return total;
	}
	
	
}
