package models;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import resources.Utils;

import com.avaje.ebean.Ebean;

@Entity
@Table(name = "votes")
public class Vote extends Model {

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
	private Date date;

	@ManyToOne
	private User user;
	
	@ManyToOne
	@Required
	private Restaurant restaurant;

	public Vote() {
		super();
	}

	public Vote(Date date, User user, Restaurant restaurant) {
		super();
		this.date = date;
		this.user = user;
		this.restaurant = restaurant;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	

	/* 
	 * Static Resources 
	 */ 

	public static Model.Finder<String, Vote> find = new Model.Finder<String, Vote>(
			String.class, Vote.class);

	public static List<Vote> findByDate(Date date) {
		return find.where().eq("date", date).findList();
	}

	public static List<Vote> findByRestaurant(Restaurant restaurant) {
		return find.where().eq("restaurant", restaurant).findList();
	}

	public static List<Vote> findByRestaurantAndCycle(Restaurant restaurant) {
		Calendar calInit = Utils.getVoteCycleInit();
		Calendar calEnd = Utils.getVoteCycleEnd();
		
		return find.where().eq("restaurant", restaurant).between("date", calInit.getTime(), calEnd.getTime()).findList();
	}

	public static Boolean userCanVote(User user) {
		Calendar calInit = Utils.getVoteCycleInit();
		Calendar calEnd = Utils.getVoteCycleEnd();
		
		List<Vote> votes;
		votes = find.where().eq("user", user).between("date", calInit.getTime(), calEnd.getTime()).findList();
		final Boolean canVote = (votes.size() == 0);

		return canVote;
	}
	
	public static List<Vote> listToManager() {
		Calendar calInit = Utils.getVoteCycleInit();
		Calendar calEnd = Utils.getVoteCycleEnd();
		
		return Ebean.find(Vote.class).fetch("restaurant").fetch("user").where().between("date", calInit.getTime(), calEnd.getTime()).findList();
	}

}
