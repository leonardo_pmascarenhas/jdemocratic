package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.data.format.Formats;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import resources.Utils;

import com.avaje.ebean.Ebean;

@Entity
@Table(name = "winners")
public class Winner extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date date;

	@ManyToOne
	@Required
	private Restaurant restaurant;

	private int votes;

	public Winner() {
		super();
	}

	public Winner(Date date, Restaurant restaurant, int votes) {
		super();
		this.date = date;
		this.restaurant = restaurant;
		this.votes = votes;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public int getVotes() {
		return votes;
	}

	public void setVotes(int votes) {
		this.votes = votes;
	}



	/* 
	 * Static Resources 
	 */ 

	public static class Candidate {
		private Restaurant restaurant;
		private int amount;

		public Candidate(Restaurant restaurant, int amount) {
			super();
			this.restaurant = restaurant;
			this.amount = amount;
		}
		public Restaurant getRestaurant() {
			return restaurant;
		}
		public int getVotes() {
			return amount;
		}
	}
	
	public static List<Restaurant> winnersOfWeek() {
		Calendar calInit = Utils.getLastWeekDate();
		Calendar calEnd = Utils.getElectionCycleEnd();

		List<Winner> winners;
		winners = Ebean.find(Winner.class).where().between("date", calInit.getTime(), calEnd.getTime()).findList();
		
		List<Restaurant> restaurants = new ArrayList<Restaurant>();
		for (Winner winner: winners) {
			restaurants.add(winner.getRestaurant());
		}
		
		return restaurants;
	}

	public static Candidate getWinner() {
		// Get the winners that had won in the last week
		List<Restaurant> winners = winnersOfWeek();
		
		List<Restaurant> restaurants = Restaurant.all();

		List<Candidate> candidates = new ArrayList<Candidate>();
		int currCount = 0;
		for (Restaurant restaurant: restaurants) {
			if (winners.contains(restaurant)) {
				//System.out.println("[Ignorado] " + restaurant.getName());
				continue;
			}
			
			currCount = restaurant.voteCounter();

			// If he don't have a vote, he is not greater than no one
			if (currCount == 0) {
				continue;
			}

			if ((candidates.isEmpty() || currCount > candidates.get(0).getVotes())) {
				Candidate candidate = new Candidate(restaurant, restaurant.voteCounter());
				candidates.clear();
				candidates.add(candidate);
				//System.out.println("[Adicionardo G] " + currCount + " = " + restaurant.getName());
				continue;
			}

			if (candidates.get(0).getVotes() == currCount) {
				Candidate win = new Candidate(restaurant, restaurant.voteCounter());
				//System.out.println("[Adicionardo E] " + currCount + " = " + restaurant.getName());
				candidates.add(win);
			}
		}

		if (candidates.size() == 0) {
			return null;
		}

		if (candidates.size() > 1) {
			Random r = new Random();
			int low = 1;
			int high = candidates.size();
			int result = r.nextInt(high-low) + low;

			return candidates.get(result);
		}

		return candidates.get(0);
	}
	
	public static Winner getWinnerDay() {
		Calendar calInit = Utils.getElectionCycleInit();
		Calendar calEnd = Utils.getElectionCycleEnd();

		List<Winner> winners = Ebean.find(Winner.class).where().between("date", calInit.getTime(), calEnd.getTime()).findList();
		
		if (winners.isEmpty()) {
			return null;
		}
		if (winners.size() > 1) {
			//This can be a future improvement
			System.out.println("[Danger] More than one winner a day. Feature not available");
		}
		return winners.get(0);
	}

	public static void election() {
		System.out.println("Checking voting cycle...");

		//Important: the election cycle is between 11:01 and 23:59
		Calendar calNow = Calendar.getInstance();
		calNow.setTime(new Date());

		Calendar calInit = Utils.getElectionCycleInit();
		Calendar calEnd = Utils.getElectionCycleEnd();
		
		// Check if the current time is in the election cycle
		if (calNow.after(calInit) && calNow.before(calEnd)) {

			// Check if the last cycle has a winner
			if (getWinnerDay() == null) {
				
				Candidate vote = getWinner();
				// Let it go, just to not try getWinner every time
                if (vote == null) { 
                	Winner winner = new Winner(new Date(), null, 0);
                	return;
                }
	
				Winner winner = new Winner(new Date(), vote.getRestaurant(), vote.getVotes());
				winner.save();
	
				System.out.println("[New Winner] " + winner.getRestaurant().getName());
			}
		}
	}
}
